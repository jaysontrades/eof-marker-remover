﻿using System.IO;
using System.Linq;

namespace EOF_Marker_Remover
{
    class Program
    {
        static void Main(string[] args)
        {
            var files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.txt");
            foreach (var file in files)
            {
                var lines = File.ReadLines(file).ToList();
                if (lines.Count > 1)
                {
                    lines.RemoveAt(lines.Count - 1);
                    File.Delete(file);
                    using (var newFile = new System.IO.StreamWriter(file))
                    {
                        foreach (var line in lines)
                        {
                            newFile.WriteLine(line);
                        }
                    }
                }
            }
        }
    }
}

